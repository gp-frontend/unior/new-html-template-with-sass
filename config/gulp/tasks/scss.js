let gcmq = require('gulp-group-css-media-queries'),
    stylesPath = {
      'src': 'src/web/styles/*.scss',
      'srcCSS': 'build/css/*.css',
      'dest': 'build/css'
    };


// PostCSS Plugins
// [1] - Generate normalize base on .browserlistrc
// [2] - Generate boostrap grid 4
// [3] - CSS Nano remove all spaces and comments
// [4] - CSSNEXT enable autoprefixer

const processors = [
  require('postcss-normalize'), // [1]
  require("postcss-bootstrap-4-grid")({ // [2]
    gridColumns: 12,
    gridGutterWidth: '30px',
    containerMaxWidths: {
      sm: '738px',
      lg: '950px',
      xl: '1170px',
      xxxl: '1620px'
    },
    gridBreakpoints: {
      xs: '600px',
      sm: '768px',
      md: '980px',
      lg: '1024px',
      xl: '1200px',
      xxl: '1600px',
      xxxl: '1920px',
    }
  }),
  require('postcss-pxtorem')({
    rootValue: 16,
    propList: ['font', 'font-size'],
    mediaQuery: false,
    minPixelValue: 0
  }),
  require('cssnano')({ // [3]
    autoprefixer: false,
    discardComments: { removeAll: true }
  }),
  require('postcss-cssnext'), // [4]
];

module.exports = () => {

  // DEV TASK
  $.gulp.task('styles:dev', () => {

    return $.gulp.src(stylesPath.src)

      .pipe($.plugins.plumber({

        errorHandler: function(err) {

         $.plugins.notify.onError({
            title: "Error in SCSS file",
            message: "<%= error.message %>"
          })(err);
        }
      }))

      .pipe($.plugins.sourcemaps.init())

      .pipe($.plugins.sass().on('error', $.plugins.sass.logError))

      .pipe($.plugins.postcss(processors))

      .pipe(gcmq())

      .pipe($.plugins.sourcemaps.write())

      .pipe($.plugins.rename({
          extname: '.min.css'
        }),
      )

      .pipe($.gulp.dest(stylesPath.dest))

      .pipe($.browserSync.stream());
  });

  // MINIFY TASK
  $.gulp.task('styles:minify', () => {

    return $.gulp.src(stylesPath.srcCSS)

      .pipe($.plugins.cssnano({
        autoprefixer: false,
      }))

      .pipe($.gulp.dest(stylesPath.dest));
  });
};
